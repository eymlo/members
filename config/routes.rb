Members::Application.routes.draw do
  root 'check_in#checkin_page'

  get 'checkin', to: 'check_in#checkin_page'
  get 'checkin/histories', to: 'check_in#histories'
  post 'checkin', to: 'check_in#checkin'

  resources 'checkin', controller: "check_in", only: [:edit, :destroy, :update], as: "check_in"

  resources 'members', only: [:index, :show, :update, :create, :new, :destroy] do
    get 'edit', to: 'members#edit'
    get 'destroy_confirm'

    resources 'membership_registrations', only: [:create, :edit, :update, :new]

    delete 'avatar', to: 'avatar#destroy', as: :destroy_avatar
  end

  resources 'activities', only: [:index, :show, :update, :create, :new] do
    get 'edit', to: 'activites#edit'
  end

  resources 'sessions', only: [:create, :destroy, :new]
  get 'sessions/signout', to: 'sessions#destroy'

  resources 'course_instructors', only: [:index, :show, :update, :create, :new, :edit] do
  end

  get 'courses/calendar', to: 'courses#calendar'
  get 'courses/today', to: 'courses#today'
  get 'courses/:course_id/duplicate', to: 'courses#duplicate', as: :course_duplicate

  resources 'courses' do
    resources 'course_attendances', only: [:edit, :show, :update, :destroy]

    get 'member/:id/course_class/edit', to: 'course_class_memberships#edit', as: :edit_membership
    post 'member/:id/course_class', to: 'course_class_memberships#update', as: :update_membership

    resources 'course_classes', only: [:edit, :update, :destroy, :show, :new, :create]

    post 'update_attendance'
    post 'regenerate_classes'
  end

  get 'email_list_generator', :controller => :utils, :action => :email_list_generator

  resources 'course_registrations' do
    collection do
      get 'new_registration'
      post 'create_registration'
    end
  end

  resources 'books' do
    collection do
      get 'new_registration'
      post 'create_registration'
    end
    member do
      get 'mark_as_returned'
    end
  end

  resources 'book_rentals', :only => [:edit, :update, :create, :new] do
    collection do
      get 'return_a_book'
      post 'return_a_book', :to => 'book_rentals#do_return'
    end
  end
end

