Members::Config = Hashie::Mash.new
Members::Config.merge!(YAML.load_file('config/application.yml')[Rails.env])
