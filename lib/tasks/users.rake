require 'csv'

fields = {
  street: 4,
  city: 5,
  postal_code: 6,
  home_phone: 7,
  cell_phone: 8,
  email_address: 9,
  emergency_contact: 10,
  emergency_telephone_number: 11,
  emergency_contact_relationship: 12,
  date_of_birth: 13,
  notes: 14 }

desc 'Import Users from CSV'
namespace :user do
  task :import, [:filename] => :environment do | t, args |
    puts t.inspect
    puts args.inspect
    Member.transaction do
      CSV.foreach(args[:filename]) do | row |
        Member.create({ card_number: row[0],
          first_name: row[1],
          last_name: row[2],
          chinese_name: row[3],
          registration_date: DateTime.parse("#{row[4]} #{Time.zone}"),
          latest_expiry_date: DateTime.parse("#{row[5]} #{Time.zone}")})
      end
    end
  end

  desc 'Load User extra info'
  task :load, [:filename] => :environment do |t, args|
    puts t.inspect
    puts args.inspect
    Member.transaction do
      CSV.foreach(args[:filename]) do | row |
        m = Member.find_by(card_number: row[0])
        if m
          fields.each do |k,v|
            m[k] = row[v]
          end
          m.save!
          puts "Card number #{row[0]}"
        else
          puts "Card number #{row[0]} cannot be found"
        end
      end
    end
  end

  desc 'Load User registration'
  task :load_registration, [:filename] => :environment do |t, args|
    puts t.inspect
    puts args.inspect
    Member.transaction do
      CSV.foreach(args[:filename]) do | row |
        m = Member.find_by(card_number: row[0])
        if m
          registration_date = row[15]
          year_2012_receipt = row[18]
          year_2013_expiry = row[19]

          year_2013_renewal = row[20]
          year_2013_receipt = row[21]
          year_2014_expiry = row[22]

          year_2014_renewal = row[23]
          year_2014_receipt = row[24]
          year_2015_expiry = row[25]

          if year_2012_receipt
            MembershipRegistration.new({year: 2012, member: m,
                                       registration_date: registration_date,
                                       receipt_number: year_2012_receipt,
                                       new_expiry_date: year_2013_expiry}).save!
          end

          if year_2013_receipt
            MembershipRegistration.new({year: 2013,
                                       member: m,
                                       registration_date: year_2013_renewal.nil? ? registration_date : year_2013_renewal,
                                       receipt_number: year_2013_receipt,
                                       new_expiry_date: year_2014_expiry}).save!
          end

          if year_2014_receipt
            MembershipRegistration.new({year: 2014, member: m,
                                       registration_date: year_2014_renewal.nil? ? registration_date : year_2014_renewal,
                                       receipt_number: year_2014_receipt,
                                       new_expiry_date: year_2015_expiry}).save!
          end

          puts "Card number #{row[0]}"
          m.save!
        else
          puts "Card number #{row[0]} cannot be found"
        end
      end
    end
  end

end
