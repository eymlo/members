expiry = Date.parse('2013-01-01').to_time
FIELDS = ['card_number', 'last_name', 'first_name', 'chinese_name', 'street', 'city', 'postal_code', 'home_phone', 'cell_phone', 'date_of_birth', 'email_address', 'emergency_contact', 'emergency_telephone_number', 'emergency_contact_relationship', 'registration_date']
begin
Member.transaction do
  data = CSV.read('dt.csv')
  puts data.count
  data.each_with_index do |row, i|
    next if i == 0
    puts "#{i} out of #{data.count}"

    d = {}
    FIELDS.each_with_index do |f, index|
      d[f] = row[index]
    end
    d['latest_expiry_date'] = expiry
    d['registration_date'] = Date.parse(d['registration_date'])
    member = Member.create!(d)
    MembershipRegistration.create!(member: member,
                                   new_expiry_date: expiry,
                                   registration_date: member.registration_date,
                                   receipt_number: row[FIELDS.count],
                                   year: 2014)
  end
end
rescue => e
  puts e.to_s
end
