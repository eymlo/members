FIELDS = [
  'book_code',
  'name',
  'author',
  'publisher',
  'isbn',
  'remarks'
]
begin
  Book.transaction do
    data = CSV.read('books.csv')
    puts data.count
    data.each_with_index do |row, i|
      next if i == 0
      puts "#{i} out of #{data.count}"

      d = {}
      FIELDS.each_with_index do |f, index|
        d[f] = row[index]
      end
      Book.create!(d)
    end
  end
rescue => e
  puts e.to_s
end
