# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151017011422) do

  create_table "activities", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "location_id"
  end

  add_index "activities", ["location_id"], name: "index_activities_on_location_id", using: :btree

  create_table "book_rentals", force: true do |t|
    t.integer  "book_id",     null: false
    t.integer  "member_id",   null: false
    t.datetime "borrowed_at", null: false
    t.datetime "expiring_at", null: false
    t.datetime "returned_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "book_rentals", ["book_id"], name: "index_book_rentals_on_book_id", using: :btree
  add_index "book_rentals", ["member_id"], name: "index_book_rentals_on_member_id", using: :btree

  create_table "books", force: true do |t|
    t.string   "name",        default: "",    null: false
    t.string   "book_code",   default: "",    null: false
    t.string   "description"
    t.string   "author"
    t.string   "isbn"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "publisher"
    t.text     "remarks"
    t.boolean  "available",   default: true,  null: false
    t.boolean  "lost_damage", default: false, null: false
  end

  add_index "books", ["book_code"], name: "index_books_on_book_code", unique: true, using: :btree

  create_table "check_ins", force: true do |t|
    t.integer  "member_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "activity_id"
  end

  create_table "course_attendances", force: true do |t|
    t.integer  "member_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "course_class_id"
    t.boolean  "deleted",         default: false
  end

  add_index "course_attendances", ["member_id"], name: "index_course_attendances_on_member_id", using: :btree

  create_table "course_class_memberships", force: true do |t|
    t.integer  "member_id"
    t.integer  "course_class_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "course_class_memberships", ["course_class_id"], name: "index_course_class_memberships_on_course_class_id", using: :btree
  add_index "course_class_memberships", ["member_id"], name: "index_course_class_memberships_on_member_id", using: :btree

  create_table "course_classes", force: true do |t|
    t.datetime "start_time"
    t.datetime "end_time"
    t.integer  "course_id"
    t.boolean  "cancelled",  default: false
  end

  add_index "course_classes", ["cancelled", "start_time"], name: "index_course_classes_on_cancelled_and_start_time", using: :btree
  add_index "course_classes", ["course_id"], name: "index_course_classes_on_course_id", using: :btree

  create_table "course_instructors", force: true do |t|
    t.string   "name"
    t.string   "chinese_name"
    t.string   "info"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "course_registrations", force: true do |t|
    t.integer  "member_id"
    t.integer  "course_id"
    t.decimal  "amount_paid",    precision: 5, scale: 2
    t.integer  "receipt_number"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "remarks"
  end

  add_index "course_registrations", ["course_id"], name: "index_course_registrations_on_course_id", using: :btree
  add_index "course_registrations", ["member_id"], name: "index_course_registrations_on_member_id", using: :btree

  create_table "courses", force: true do |t|
    t.string   "course_code"
    t.string   "course_name"
    t.string   "course_chinese_name"
    t.text     "course_description"
    t.date     "start_date"
    t.date     "end_date"
    t.integer  "day_of_week",              limit: 1
    t.integer  "number_of_lessons",        limit: 1
    t.integer  "course_fee_general",       limit: 1
    t.integer  "course_fee_senior_circle", limit: 1
    t.integer  "course_fee_volunteer",     limit: 1
    t.integer  "course_instructor_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.time     "start_time"
    t.time     "end_time"
    t.integer  "location_id"
  end

  add_index "courses", ["course_code"], name: "index_courses_on_course_code", using: :btree
  add_index "courses", ["course_instructor_id"], name: "index_courses_on_course_instructor_id", using: :btree
  add_index "courses", ["location_id"], name: "index_courses_on_location_id", using: :btree

  create_table "locations", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "locations", ["name"], name: "index_locations_on_name", unique: true, using: :btree

  create_table "members", force: true do |t|
    t.string   "card_number"
    t.string   "last_name"
    t.string   "first_name"
    t.string   "chinese_name"
    t.string   "street"
    t.string   "city"
    t.string   "postal_code"
    t.string   "home_phone"
    t.string   "cell_phone"
    t.string   "email_address"
    t.string   "emergency_contact"
    t.string   "emergency_telephone_number"
    t.string   "emergency_contact_relationship"
    t.string   "date_of_birth",                  limit: 7
    t.boolean  "issued_membership_card",                   default: true
    t.string   "notes"
    t.date     "registration_date"
    t.date     "latest_expiry_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

  add_index "members", ["card_number"], name: "index_members_on_card_number", unique: true, using: :btree

  create_table "membership_registrations", force: true do |t|
    t.integer  "member_id"
    t.date     "registration_date"
    t.date     "new_expiry_date"
    t.integer  "receipt_number"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "year"
  end

  create_table "versions", force: true do |t|
    t.string   "item_type",  default: "", null: false
    t.integer  "item_id",                 null: false
    t.string   "event",      default: "", null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree

end
