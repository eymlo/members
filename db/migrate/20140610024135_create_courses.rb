class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.string :course_code
      t.string :course_name
      t.string :course_chinese_name
      t.text :course_description
      t.date :start_date
      t.date :end_date
      t.integer :day_of_week, limit: 1
      t.integer :number_of_lessons, limit: 1
      t.integer :course_fee_general, limit: 1
      t.integer :course_fee_senior_circle, limit: 1
      t.integer :course_fee_volunteer, limit: 1
      t.references :course_instructor, index: true
      t.timestamps
    end
  end
end
