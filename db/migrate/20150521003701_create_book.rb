class CreateBook < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.string :name, :null => false
      t.string :book_code, :null => false
      t.string :description
      t.string :author
      t.string :isbn

      t.index :book_code, :unique => true
      t.timestamps
    end
  end
end
