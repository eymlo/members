class ChangeDateTimeToDateInMembers < ActiveRecord::Migration
  def change
    change_column :members, :registration_date, :date
    change_column :members, :latest_expiry_date, :date
  end
end
