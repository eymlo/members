class AddUniqueIndexToCardNumber < ActiveRecord::Migration
  def change
    add_index :members, :card_number, unique: true
  end
end
