class CreateBookRental < ActiveRecord::Migration
  def change
    create_table :book_rentals do |t|
      t.integer :book_id, :null => false
      t.integer :member_id, :null => false
      t.timestamp :borrowed_at, :null => false
      t.timestamp :expiring_at, :null => false
      t.timestamp :returned_at

      t.index :book_id
      t.index :member_id
      t.timestamps
    end
  end
end
