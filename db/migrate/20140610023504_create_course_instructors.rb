class CreateCourseInstructors < ActiveRecord::Migration
  def change
    create_table :course_instructors do |t|
      t.string :name
      t.string :chinese_name
      t.string :info
      t.timestamps
    end
  end
end
