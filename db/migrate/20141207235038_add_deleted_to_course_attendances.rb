class AddDeletedToCourseAttendances < ActiveRecord::Migration
  def change
    add_column :course_attendances, :deleted, :boolean, default: false
  end
end
