class CreateMembers< ActiveRecord::Migration
  def change
    create_table :members do |t|
      t.string :card_number
      t.string :last_name
      t.string :first_name
      t.string :chinese_name
      t.string :street
      t.string :city
      t.string :postal_code
      t.string :home_phone
      t.string :cell_phone
      t.string :email_address
      t.integer :raiser_edge_number
      t.string :emergency_contact
      t.string :contact_telephone_number
      t.string :relationship
      t.date :date_of_birth
      t.boolean :issued_membership_card, default: true
      t.string :remarks
      t.timestamp :registration_date
      t.timestamp :latest_expiry_date
      t.timestamps
    end
  end
end
