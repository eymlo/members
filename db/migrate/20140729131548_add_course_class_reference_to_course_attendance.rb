class AddCourseClassReferenceToCourseAttendance < ActiveRecord::Migration
  def change
    remove_column :course_attendances, :course_id, :integer, index: true
    add_column :course_attendances, :course_class_id, :integer, index: true, references: :course_class
  end
end
