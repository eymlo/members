class AddAvailableToBooks < ActiveRecord::Migration
  def change
    add_column :books, :available, :boolean, :default => true, :null => false
  end
end
