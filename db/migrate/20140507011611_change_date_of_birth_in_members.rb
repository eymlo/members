class ChangeDateOfBirthInMembers < ActiveRecord::Migration
  def change
    Member.all.each do | member |
      member.date_of_birth = nil
      member.save!
    end
    change_column :members, :date_of_birth, :string, limit: 7
  end
end
