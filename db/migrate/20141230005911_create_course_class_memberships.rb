class CreateCourseClassMemberships < ActiveRecord::Migration
  def change
    create_table :course_class_memberships do |t|
      t.references :member, index: true
      t.references :course_class, index: true
      t.timestamps
      t.timestamps
    end

    CourseRegistration.all.each do |reg|
      reg.course.course_classes.each do |c|
        CourseClassMembership.create!(member: reg.member, course_class: c)
      end
    end
  end
end
