class ChangeDateColumnsInMemberRegistrations < ActiveRecord::Migration
  def change
    change_column :membership_registrations, :registration_date, :date
    change_column :membership_registrations, :new_expiry_date, :date
  end
end
