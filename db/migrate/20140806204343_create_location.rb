class CreateLocation < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :name
      t.timestamps
    end
    add_index :locations, :name, unique: true

    add_column :courses, :location_id, :integer
    add_index :courses, :location_id
    add_column :activities, :location_id, :integer
    add_index :activities, :location_id

    ActiveRecord::Base.connection.execute("
INSERT INTO `locations` (`id`, `name`)
VALUES
    (1, 'scarborough'),
    (2, 'downtown');
                                          ")
    ActiveRecord::Base.connection.execute("UPDATE activities SET location_id = 1")
  end
end
