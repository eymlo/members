class CreateCourseAttendances < ActiveRecord::Migration
  def change
    create_table :course_attendances do |t|
      t.references :course, index: true
      t.references :member, index: true
      t.timestamps
    end
  end
end
