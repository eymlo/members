class CreateCourseRegistrations < ActiveRecord::Migration
  def change
    create_table :course_registrations do |t|
      t.references :member, index: true
      t.references :course, index: true
      t.decimal :amount_paid, precision: 5, scale: 2
      t.integer :receipt_number
      t.timestamps
    end
  end
end
