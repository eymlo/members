class AddYearToMembershipRegistrations < ActiveRecord::Migration
  def change
    add_column :membership_registrations, :year, :integer
  end
end
