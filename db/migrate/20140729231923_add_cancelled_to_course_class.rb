class AddCancelledToCourseClass < ActiveRecord::Migration
  def change
    add_column :course_classes, :cancelled, :boolean, default: false
    add_index :course_classes, [:cancelled, :start_time]
  end
end
