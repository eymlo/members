class AddRemarksToCourseRegistration < ActiveRecord::Migration
  def change
    add_column :course_registrations, :remarks, :string
  end
end
