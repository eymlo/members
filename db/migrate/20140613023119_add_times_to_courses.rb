class AddTimesToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :start_time, :time
    add_column :courses, :end_time, :time
    add_index :courses, :course_code
  end
end
