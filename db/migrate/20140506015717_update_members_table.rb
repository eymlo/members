class UpdateMembersTable < ActiveRecord::Migration
  def change
    rename_column :members, :remarks, :notes
    remove_column :members, :raiser_edge_number, :string
    rename_column :members, :contact_telephone_number, :emergency_telephone_number
    rename_column :members, :relationship, :emergency_contact_relationship
  end
end
