class CreateCourseClass < ActiveRecord::Migration
  def change
    create_table :course_classes do |t|
      t.datetime :start_time, index: true
      t.datetime :end_time

      t.references :course, index: true
    end
  end
end
