class CreateMembershipRegistration < ActiveRecord::Migration
  def change
    create_table :membership_registrations do |t|
      t.references :member
      t.datetime :registration_date
      t.datetime :new_expiry_date
      t.integer :receipt_number
      t.timestamps
    end
  end
end
