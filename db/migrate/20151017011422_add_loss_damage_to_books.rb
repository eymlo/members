class AddLossDamageToBooks < ActiveRecord::Migration
  def change
    add_column :books, :lost_damage, :boolean, :default => false, :null => false
  end
end
