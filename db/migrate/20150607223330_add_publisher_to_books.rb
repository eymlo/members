class AddPublisherToBooks < ActiveRecord::Migration
  def change
    add_column :books, :publisher, :string
    add_column :books, :remarks, :text
  end
end
