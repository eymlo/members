class Member < ActiveRecord::Base
  MEMBER_TYPES_MAPPING = {
    SC: 'Scarborough Senior Circle',
    DT: 'Downtown Senior Circle',
    V: 'Volunteer',
    SR: 'Scarborough Regular',
    DR: 'Downtown Regular',
  }

  has_paper_trail
  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.jpg"

  XLS_FIELDS = [:card_number, :last_name, :first_name, :chinese_name, :issued_membership_card, :registration_date, :latest_expiry_date, :street, :city, :postal_code, :home_phone, :cell_phone, :email_address, :emergency_contact, :emergency_telephone_number, :emergency_contact_relationship, :date_of_birth]

  validates :card_number, presence: true, uniqueness: true
  validates :last_name, presence: true
  validates :first_name, presence: true
  validates :issued_membership_card, :inclusion => { :in => [true, false] }
  validates :registration_date, presence: true
  validates :latest_expiry_date, presence: true
  validate :valid_member_type?
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

  has_many :check_ins, inverse_of: :member, :dependent => :delete_all
  has_many :registrations,
    inverse_of: :member,
    class_name: 'MembershipRegistration',
    dependent: :delete_all
  has_many :course_registrations,
    inverse_of: :member,
    dependent: :delete_all
  has_many :courses,
    through: :course_registrations
  has_many :course_classes,
    class_name: 'CourseClassMembership',
    inverse_of: :member,
    dependent: :delete_all
  has_many :course_attendances,
    inverse_of: :member,
    dependent: :delete_all
  has_many :book_rentals,
    inverse_of: :member
  validates_associated :check_ins

  before_create :card_number_uppercase
  before_save :sanitize_name

  def valid_member_type?
    if self.member_type.nil?
      self.errors['card_number'] = "#{card_number} has an invalid member type"
    end
  end

  def card_number_uppercase
    self.card_number.upcase!
  end

  def sanitize_name
    self.last_name = self.last_name.titleize if self.last_name.present?
    self.first_name = self.first_name.titleize if self.first_name.present?
  end

  def fullname
    "#{first_name.split(" ").map {|x|x.capitalize }.join(" ")} #{last_name.split(" ").map {|x| x.capitalize }.join(" ")}"
  end

  def expired?(date = Time.zone.now.beginning_of_day)
    self.latest_expiry_date < date
  end

  def to_xls
    date = DateTime.now
    book = Spreadsheet::Workbook.new
    sheet1 = book.create_worksheet name: 'Member Info'

    sheet1.row(0).replace(XLS_FIELDS.map {|x| x.to_s.split('_').map {|s| s.capitalize}.join(' ')})
    sheet1.row(1).replace(XLS_FIELDS.map {|x| self[x]})

    sheet2 = book.create_worksheet name: 'Member Activities'
    sheet2.row(0).replace(['Time', 'Activity Name'])

    index = 1
    self.check_ins.order(created_at: :asc).each do |c|
      sheet2.row(index).replace([c.created_at, c.activity.name])
      index += 1
    end

    sheet3 = book.create_worksheet name: 'Renewal Histories'
    sheet3.row(0).replace(MembershipRegistration::XLS_FIELDS.map {|x| x.to_s.split('_').map{|y| y.capitalize}.join(' ') })

    index = 1
    self.registrations.order(created_at: :asc).each do |c|
      sheet3.row(index).replace(MembershipRegistration::XLS_FIELDS.map {|field| c[field]})
      index += 1
    end

    return "#{self.card_number}_#{date.strftime('%Y%m%d')}.xls", book
  end

  def self.to_xls(members = nil)
    date = DateTime.now
    book = Spreadsheet::Workbook.new
    sheet1 = book.create_worksheet name: 'Member Info'
    members = Member.all if members.nil?

    sheet1.row(0).replace(XLS_FIELDS.map {|x| x.to_s.split('_').map {|s| s.capitalize}.join(' ')})
    index = 1
    members.each do | member |
      sheet1.row(index).replace(XLS_FIELDS.map {|x| member[x]})
      index += 1
    end

    sheet3 = book.create_worksheet name: 'Renewal Histories'
    years = MembershipRegistration.where(:member_id => members.map(&:id)).select(:year).distinct.order(:year).map(&:year)
    header = ['Member Card Number', 'Member name'] + MembershipRegistration::XLS_FIELDS.map {|x| x.to_s.split('_').map{|y| y.capitalize}.join(' ') } * years.count
    sheet3.row(0).replace(header)

    index = 1
    MembershipRegistration.includes(:member).where(:member_id => members.map(&:id)).
      order(member_id: :asc).group_by(&:member).each do |member, regs|
      row = [member.card_number, member.fullname]
      years.each do |year|
        reg = regs.find{|r| r.year ==  year} || {}
        row += MembershipRegistration::XLS_FIELDS.map {|field| reg[field]}
      end

      sheet3.row(index).replace(row)

      index += 1
    end

    return "all_members_#{date.strftime('%Y%m%d')}.xls", book
  end

  def member_type_string
    MEMBER_TYPES_MAPPING[card_number.split('-').first.to_sym]
  end

  def member_type
    type = card_number.split('-').first.to_sym
    MEMBER_TYPES_MAPPING.keys.include?(type) ? type : nil
  end
end

