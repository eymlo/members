class Activity < ActiveRecord::Base
  has_paper_trail
  has_many :check_ins, inverse_of: :activity
  before_validation :sanitize_name
  validates :name, presence: true, uniqueness: true
  belongs_to :location, inverse_of: :activities
  validates :location, presence: true

  scope :at_location, ->(location) { location.present? ? includes(:location).where(locations: {name: location}) : all }

  def sanitize_name
    self.name.capitalize!
  end

  def self.to_xls
    date = DateTime.now
    book = Spreadsheet::Workbook.new
    sheet1 = book.create_worksheet name: 'Activities'

    sheet1.row(0).replace(['Name'])
    index = 1
    Activity.all.each do |c|
      sheet1.row(index).replace([c.name])
      index += 1
    end

    return "activities_#{date.strftime('%Y%m%d')}.xls", book
  end
end
