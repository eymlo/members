module Export
  def export_xls(book = nil, spec = nil)
    book = Spreadsheet::Workbook.new if book.nil?

    sheet1 = book.create_worksheet name: self.name

    join_fields = self::EXPORT_FIELDS.select {|x| x.is_a? Hash}.map(&:keys).flatten.uniq

    sheet1.row(0).replace(build_header(self::EXPORT_FIELDS))

    query = self.all.joins(join_fields)
    if spec
      query = query.where(spec)
    end

    query.each_with_index do |data_row, index|
      row = []
      self::EXPORT_FIELDS.each do |field|
        row += extract_values(data_row, field)
      end

      sheet1.row(index + 1).replace(row)
    end

    book
  end

  private

  def extract_values(obj, field)
    values = []
    if field.is_a? Hash
      field.each do |model, sub_fields|
        sub_fields.each do |f|
          values << obj.send(model).try(:send, f)
        end
      end
    else
      values << obj.send(field)
    end

    values
  end

  def build_header(fields)
    header = []
    fields.each do |field|
      if field.is_a? Hash
        field.each do |model, values|
          values.each do |value|
            header << "#{model.to_s.titleize} #{value.to_s.titleize}"
          end
        end
      else
        header << "#{field.to_s.titleize}"
      end
    end

    header
  end
end

