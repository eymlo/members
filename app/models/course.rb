class Course < ActiveRecord::Base
  has_paper_trail
  module DayOfWeek
    SUNDAY = 0
    MONDAY = 1
    TUESDAY = 2
    WEDNESDAY = 3
    THURSDAY = 4
    FRIDAY = 5
    SATURDAY = 6

    WEEK = { 0 => 'Sunday',
                1 => 'Monday',
                2 => 'Tuesday',
                3 => 'Wednesday',
                4 => 'Thursday',
                5 => 'Friday',
                6 => 'Saturday'}

    def self.for(input)
      if input.is_a? String
        (WEEK.invert)[input]
      else
        WEEK[input]
      end
    end
  end

  CLONABLE_FIELDS = [:course_name,
                     :course_chinese_name,
                     :course_description,
                     :day_of_week,
                     :start_time,
                     :end_time,
                     :number_of_lessons,
                     :course_fee_general,
                     :course_fee_senior_circle,
                     :course_fee_volunteer,
                     :course_instructor_id,
                     :location_id]

  scope :has_class_on, ->(datetime) { where('start_date <= ?', datetime).where('end_date >= ?', datetime).where(day_of_week: datetime.wday)}
  scope :at_location, ->(location) { location.present? ? includes(:location).where(locations: {name: location}) : all }

  validates :course_code, presence: true, uniqueness: true
  validates :course_name, presence: true
  validates :course_chinese_name, presence: true
  validates :course_description, presence: true
  validates :start_date, presence: true
  validates :end_date, presence: true
  validates :day_of_week, presence: true, numericality: true, inclusion: DayOfWeek::WEEK.keys
  validates :start_time, presence: true
  validates :end_time, presence: true
  validates :number_of_lessons, presence: true, numericality: true
  validates :course_fee_general, presence: true, numericality: true
  validates :course_fee_senior_circle, presence: true, numericality: true
  validates :course_fee_volunteer, presence: true, numericality: true
  validates :course_instructor, presence: true
  validates :location, presence: true

  belongs_to :course_instructor, inverse_of: :courses
  belongs_to :location, inverse_of: :courses
  has_many :course_registrations,
    inverse_of: :course
  has_many :course_classes, -> { where(cancelled: false)}, inverse_of: :course
  has_many :course_attendances, through: :course_classes
  has_many :members, through: :course_registrations

  def course_display_name
    "#{self.course_code} (#{self.course_name})"
  end

  def start_time_value
    self.start_time.nil? ? '' : self.start_time.strftime("%I:%M %p")
  end

  def end_time_value
    self.end_time.nil? ? '' : self.end_time.strftime("%I:%M %p")
  end

  def day_of_week_string
    DayOfWeek.for(self.day_of_week)
  end

  def is_started?
    self.start_date <= Time.zone.now
  end

  def generate_classes
    classes = []
    start_offset = self.day_of_week - self.start_date.wday
    actual_start_date = self.start_date + (start_offset < 0 ? start_offset + 7 : start_offset).day

    end_offset = self.day_of_week - self.end_date.wday
    actual_end_date = self.end_date + (end_offset < 0 ? end_offset + 7 : end_offset).day

    ((actual_end_date.end_of_day.to_time - actual_start_date.to_time) / 7.day).ceil.times do |i|
      date = (actual_start_date + (i * 7).day).to_time.change(hour: 12).in_time_zone
      starting = date.change(hour: self.start_time.hour, min: self.start_time.min)
      ending = date.change(hour: self.end_time.hour, min: self.end_time.min)

      classes << CourseClass.create!(start_time: starting, end_time: ending, course: self)
    end
    classes
  end

  def has_too_many_classes?
    self.course_classes.count > number_of_lessons
  end

  def self.events(start_time, end_time)
    return [] unless start_time && end_time
    return [] if start_time > end_time
    start_time = start_time.to_datetime
    end_time = end_time.to_datetime

    CourseClass.active.where('start_time >= ? AND end_time <= ?', start_time, end_time).map do |course_class|
      course_class.event_json
    end
  end

  def duplicate
    Course.new(CLONABLE_FIELDS.reduce({}) { |memo, field|
        memo[field] = self[field]
        memo
    })
  end

  def to_xls
    date = DateTime.now
    book = Spreadsheet::Workbook.new

    # Sheet 1. Basic info
    sheet1 = book.create_worksheet name: 'Course Info'

    sheet1.row(0).replace(CLONABLE_FIELDS.map {|x| x.to_s.split('_').map {|s| s.capitalize}.join(' ')})
    sheet1.row(1).replace(CLONABLE_FIELDS.map {|x| self[x]})

    sheet2 = book.create_worksheet name: 'Course Members'
    sheet2.row(0).replace(
      ['Member Name',
       'Member Card Number',
       'Member Chinese Name',
       'Registration Date',
       'Amount Paid',
       'Receipt Number',
       'Remarks']
    )
    self.course_registrations.order('member_id ASC').each_with_index do |reg, index|
      member = reg.member

      row = [
        member.card_number,
        member.fullname,
        member.chinese_name,
        reg.created_at,
        reg.amount_paid,
        reg.receipt_number,
        reg.remarks
      ]

      sheet2.row(index + 1).replace(row)
    end

    classes = self.course_classes.order('start_time ASC')
    sheet3 = book.create_worksheet name: 'Course Attendance'
    sheet3.row(0).replace(
      ['Member Name',
       'Member Card Number',
       'Member Chinese Name',
       'Registration Date'] +
       classes.map(&:start_time).map(&:to_date)
    )

    self.members.order('member_id ASC').each_with_index do |member, index|
      row = [
        member.card_number,
        member.fullname,
        member.chinese_name,
      ] + classes.map do |klass|
        klass.attended?(member)
      end

      sheet3.row(index + 1).replace(row)
    end


    return "course_#{self.id}_#{self.course_name}_#{date.strftime('%Y%m%d')}.xls", book
  end
end

