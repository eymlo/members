class CourseRegistration < ActiveRecord::Base
  extend Export

  has_paper_trail
  attr_accessor :force

  belongs_to :course, inverse_of: :course_registrations
  belongs_to :member, inverse_of: :course_registrations

  validates :member, presence: true
  validates :course, presence: true
  validates_associated :member
  validates_uniqueness_of :member, scope: :course, allow_nil: true
  validates :amount_paid, numericality: true, presence: true

  validate :course_still_valid, :unless => :force
  validate :member_still_valid, on: :create, :unless => :force

  before_destroy :cleanup

  EXPORT_FIELDS = [
    {
      :course => [:course_code, :course_name, :start_date],
      :member => [:card_number, :fullname]
    }
  ]

  def cleanup
    self.member.course_attendances.includes(:course_class).where(course_classes: {course_id: self.course.id}).each {|a| a.destroy!}
    CourseClassMembership.includes(:course_class).where(
      course_classes: { course_id: self.course.id},
      member: self.member
    ).each {|a| a.destroy!}
  end

  def course_still_valid
    errors[:course] = 'has already ended' if course.end_date < Time.now
  end

  def member_still_valid
    errors[:member] = 'is expired' if member.expired?(course.start_date)
  end

  def generate_course_classes
    if CourseClassMembership.joins(:course_class).where(:member => self.member,
                                                        :course_classes => {:course_id => self.course.id}).present?
      raise 'already generated'
    end

    self.class.transaction do
      self.course.course_classes.each do |klass|
        CourseClassMembership.create!(:course_class => klass, :member => self.member)
      end
    end
  end

  def self.to_xls(workbook = nil)
    date = DateTime.now
    workbook = self.export_xls(workbook)

    return "course_member_#{date.strftime('%Y%m%d')}.xls", workbook
  end
end

