class CourseClass < ActiveRecord::Base
  has_paper_trail

  after_create :generate_course_class_membership

  validates :start_time, presence: true
  validates :end_time, presence: true
  validate :time_check
  scope :active, -> { where(cancelled: false) }
  scope :at_location, ->(location) { location.present? ? includes(course: :location).where(locations: {name: location}) : all }

  belongs_to :course, inverse_of: :course_classes
  has_many :course_attendances,
    inverse_of: :course_class
  has_many :members,
    through: :course_attendances
  has_many :class_memberships,
    class_name: '::CourseClassMembership',
    inverse_of: :course_class

  def generate_course_class_membership
    self.course.members.each do |member|
      if CourseClassMembership.where(member: member, course_class: self).blank?
        CourseClassMembership.create!(member: member,
                                     course_class: self)
      end
    end
  end

  def time_check
    errors[:start_time] = 'Start time cannot be later than end time.' if start_time > end_time
  end

  def is_member?(member)
    self.class_memberships.where(member: member).present?
  end

  def attended?(member)
    self.course_attendances.where(member: member).present?
  end

  def event_json
    {
      id: self.course.id,
      title: self.course.course_name,
      start: self.start_time.to_est.strftime("%Y-%m-%dT%H:%M%z"),
      end: self.end_time.to_est.strftime("%Y-%m-%dT%H:%M%z"),
      url: Rails.application.routes.url_helpers.course_path(self.course)
    }
  end

  def date
    self.start_time.to_date
  end

  def start_time_value
    self.start_time.nil? ? '' : self.start_time.strftime("%I:%M %p")
  end

  def end_time_value
    self.end_time.nil? ? '' : self.end_time.strftime("%I:%M %p")
  end

  def mark_as_cancelled
    self.cancelled = true
    self.save!
  end

  class << self
    def for_today(location = nil)
      CourseClass.at_location(location).active.where(start_time: DateTime.now.beginning_of_day..DateTime.now.end_of_day)
    end
  end
end
