class Location < ActiveRecord::Base
  has_paper_trail

  has_many :courses, inverse_of: :location
  has_many :activities, inverse_of: :location

  validates :name, presence: true
  before_save :normalize_name

  def normalize_name
    self.name = self.name.downcase
  end

  def display_name
    self.name.capitalize
  end
end
