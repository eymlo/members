class BookRental < ActiveRecord::Base
  extend Export

  has_paper_trail

  SUPPORTED_MEMBER_TYPES = [:SC, :DT, :V]
  RENTAL_PERIOD = 2.weeks
  BOOK_PER_MEMBER = 2

  EXPORT_FIELDS = [
    {
      :book => [:name, :book_code],
      :member => [:card_number, :fullname]
    },
    :borrowed_at,
    :expiring_at,
    :returned_at
  ]

  belongs_to :book, :inverse_of => :book_rentals
  belongs_to :member, :inverse_of => :book_rentals

  validates :borrowed_at, :presence => true
  validates :expiring_at, :presence => true
  validates :book, :presence => true
  validates :member, :presence => true

  scope :borrowed_by, ->(member_id) { where(:member_id => member_id) }
  scope :expired, -> { where('expiring_at > NOW()') }
  scope :non_expired, -> { where('expiring_at < NOW()') }
  scope :on_loan, -> { where('returned_at IS NULL') }

  before_create :validate_member_type
  before_create :validate_book_status
  before_create :validate_member_rental_count
  after_save :_update_book

  def validate_member_type
    unless SUPPORTED_MEMBER_TYPES.include? self.member.member_type
      self.errors[:member] = "Only #{SUPPORTED_MEMBER_TYPES.inspect} members are allowed"
      return false
    end
  end

  def validate_book_status
    if self.book.book_rentals.on_loan != [self] &&
      self.book.book_rentals.on_loan != []
      self.errors[:book] = "Book is currently on loan"
      return false
    end
  end

  def validate_member_rental_count
    if self.member.book_rentals.on_loan.count >= BOOK_PER_MEMBER
      self.errors[:member] = "has already rented #{BOOK_PER_MEMBER} books"
      return false
    end
  end

  def self.to_xls(workbook = nil, book = nil)
    date = DateTime.now
    if book
      self.export_xls(workbook, :book => book)
    else
      self.export_xls(workbook)
    end

    return "book_rentals_#{date.strftime('%Y%m%d')}.xls", workbook
  end

  private

  def _update_book
    self.book.try(:save)
  end
end

