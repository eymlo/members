class Book < ActiveRecord::Base
  extend Export

  has_many :book_rentals, inverse_of: :book do
    def expired
      where('expiring_at > NOW()')
    end
    def non_expired
      where('expiring_at < NOW()')
    end
    def on_loan
      where('returned_at IS NULL')
    end
  end

  EXPORT_FIELDS = [
    :book_code, :name, :author, :publisher, :isbn, :remarks, :lost_damage
  ]

  before_validation :sanitize_name

  validates :name, presence: true
  validates :book_code, presence: true, :uniqueness => true

  before_save :_update_available

  scope :active, where(:lost_damage => false)

  def _update_available
    self.available = !is_on_loan?
    true
  end

  def sanitize_name
    self.name.capitalize!
    self.author.try(:capitalize!)
  end

  def is_on_loan?
    self.book_rentals.on_loan.present?
  end

  def mark_as_returned
    self.book_rentals.on_loan.each do |rental|
      rental.returned_at = Time.now
      rental.save!
    end
  end

  def self.to_xls(workbook = nil)
    date = DateTime.now
    workbook = self.export_xls(workbook)
    BookRental.to_xls(workbook)

    return "books_#{date.strftime('%Y%m%d')}.xls", workbook
  end
end

