class CheckIn < ActiveRecord::Base
  has_paper_trail

  belongs_to :member, inverse_of: :check_ins
  belongs_to :activity, inverse_of: :check_ins

  validates :activity, presence: true
  validates :member, presence: true
  scope :at_location, ->(location) { location.present? ? includes(activity: :location).where(locations: {name: location}) : all }

  def self.to_xls
    date = DateTime.now
    book = Spreadsheet::Workbook.new
    sheet1 = book.create_worksheet name: 'CheckIns'

    sheet1.row(0).replace(['Full Name', 'Chinese Name', 'Card Number', 'Activity Name', 'Date'])
    index = 1
    CheckIn.all.each do |c|
      sheet1.row(index).replace([c.member.fullname, c.member.chinese_name, c.member.card_number, c.activity.name, c.created_at])
      index += 1
    end

    return "checkin_history_#{date.strftime('%Y%m%d')}.xls", book
  end
end
