class CourseClassMembership < ActiveRecord::Base
  has_paper_trail

  belongs_to :course_class, inverse_of: :class_memberships
  belongs_to :member, inverse_of: :course_classes
end
