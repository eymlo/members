class CourseInstructor < ActiveRecord::Base
  has_paper_trail
  validates :name, presence: true

  has_many :courses, inverse_of: :course_instructor

  XLS_FIELDS = [:name, :chinese_name, :info]

  def self.to_xls
    date = DateTime.now
    book = Spreadsheet::Workbook.new
    sheet1 = book.create_worksheet name: 'Instructors Info'

    sheet1.row(0).replace(XLS_FIELDS.map {|x| x.to_s.split('_').map {|s| s.capitalize}.join(' ')})
    index = 1
    self.all.each do | member |
      sheet1.row(index).replace(XLS_FIELDS.map {|x| member[x]})
      index += 1
    end

    return "all_#{self.name.underscore}_#{date.strftime('%Y%m%d')}.xls", book
  end
end
