class MembershipRegistration < ActiveRecord::Base
  has_paper_trail
  belongs_to :member, inverse_of: :registrations
  XLS_FIELDS = [:year, :registration_date, :new_expiry_date, :receipt_number]

  validates :new_expiry_date, presence: true
  validates :registration_date, presence: true
  validates :member, presence: true
  validates :year, presence: true, uniqueness: { scope: :member }
end
