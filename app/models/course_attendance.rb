class CourseAttendance < ActiveRecord::Base
  has_paper_trail

  belongs_to :course_class, inverse_of: :course_attendances
  belongs_to :member, inverse_of: :course_attendances
  has_one :course, through: :course_class

  validates :member, presence: true
  validates :course_class, presence: true

  validate :member_has_registered?
  validate :created_at_is_valid?, on: :update

  scope :at_location, ->(location) { location.present? ? includes(course: :location).where(locations: {name: location}) : all }

  def member_has_registered?
    unless course_class.is_member? member
      errors[:member] = 'member is not part of this class'
    end
  end

  def created_at_is_valid?
    errors[:created_at] = "Course does not have class on #{self.created_at}" unless self.created_at.wday == self.course_class.course.day_of_week
  end
end
