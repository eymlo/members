$(function() {
    $('input#book_rental_member_card_number').change(function() {
        var card_number = $(this).val();

        if (card_number == "") {
            $('#member-info').hide();
        } else {
            $.ajax({
                url: '/members.json?card_number='  + card_number
            }).done (function (data) {
                data = data[0];
                expired = Date.parse(data['latest_expiry_date']) < Date.now();
                member_type = data['member_type'];
                $('.member-fullname').html(data['fullname']);
                $('.member-chinese-name').html(data['chinese_name']);
                $('.member-expiry-date').html(data['latest_expiry_date']);
                $('.member-avatar').attr('src', data['avatar_url']);
                $('.member-type').html(data['member_type_string']);
                $('.member-link').attr('href', '/members/' + data['id']);
                if (expired) {
                  $('.member-expiry-date').addClass('error-message');
                  $('.member-expired-notice').show();
                } else {
                  $('.member-expiry-date').removeClass('error-message');
                  $('.member-expired-notice').hide();
                }

                $('#member-info').show();
            });
        }
    });

    $('input#book_rental_book_book_code,input#book_code').change(function() {
        var book_code = $(this).val();

        if (book_code == "") {
            $('#book-info').hide();
        } else {
            $.ajax({
                url: '/books.json?book_code=' + book_code
            }).done (function (data) {
                data = data[0];
                $('.book-name').html(data['name']);
                $('.book-description').html(data['description']);
                $('.book-author').html(data['author']);
                if (data['lost']) {
                  $('.book-status').html('Lost');
                  $('.book-status').addClass('warning');
                  $('.book-status').removeClass('available');
                } else if (data['on_loan']) {
                  $('.book-status').html('Not available');
                  $('.book-status').addClass('warning');
                  $('.book-status').removeClass('available');
                } else {
                  $('.book-status').html('Available');
                  $('.book-status').removeClass('warning');
                  $('.book-status').addClass('available');
                }

                $('#book-info').show();
            });
        }
    });
});

