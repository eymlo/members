function chunkSplit(arr,size) {
  // uncomment this line if you don't want the original array to be affected
  // var arr = arr.slice();
  var arrs = [];
  while (arr.length > 0) {
    arrs.push(arr.splice(0, size));
  }
  return arrs;
}


$(document).on('ready page:load', function () {
    var member_type = '';
    var course_fee_general = '';
    var course_fee_volunteer = '';
    var course_fee_sc = '';

    var presetFee = function() {
        var target = $('input#course_registration_amount_paid');
        if (target.val() != '') {
            return
        } else {
            if (member_type != '') {
                switch(member_type) {
                    case 'SC':
                        target.val(course_fee_sc);
                        break;
                    case 'DT':
                        target.val(course_fee_sc);
                        break;
                    case 'V':
                        target.val(course_fee_volunteer);
                        break;
                    case 'SR':
                        target.val(course_fee_general);
                        break;
                    case 'DR':
                        target.val(course_fee_general);
                        break;
                }
            }
        }
    }

    $('select#course_registration_course_id').change(function() {
        var mapping = { 'course-day-of-week': 'day_of_week_string',
                        'course-name': 'course_name',
                        'course-chinese-name': 'course_chinese_name',
                        'course-from': 'start_date',
                        'course-to': 'end_date',
                        'course-from-time': 'start_time_value',
                        'course-to-time': 'end_time_value',
                        'course-fee-general': 'course_fee_general',
                        'course-fee-senior-circle': 'course_fee_senior_circle',
                        'course-fee-volunteer': 'course_fee_volunteer',
                        'number-of-lessons': 'number_of_lessons'};
        var id = $(this).val();

        if (id == "") {
            $('#course-info').hide();
        } else {
            $.ajax({
                url: '/courses/'  + id + '.json'
            }).done (function (data) {
                course_fee_volunteer = data['course_fee_volunteer'];
                course_fee_general = data['course_fee_general'];
                course_fee_sc = data['course_fee_senior_circle'];

                $.each(mapping, function(key, value) {
                    $('.' + key).html(data[value]);
                });

                $('.course-instructor').html("<a href='/course_instructors/" + data['course_instructor']['id'] + "'>" + data['course_instructor']['name'] + ' (' + data['course_instructor']['chinese_name'] + ')');
                $('#course-info').show();

                $('#course-class-body').empty();
                $.each(chunkSplit(data['course_classes'], 5), function(index, obj) {
                    $('#course-class-body').append("<tr>");
                    $.each(obj, function(index, o) {
                      var date = o['date'];
                      date = date.replace('-', '-<wbr>');
                      $('#course-class-body').append("<th>" + date + "</th>");
                    });
                    $('#course-class-body').append("</tr>");

                    $('#course-class-body').append("<tr>");
                    $.each(obj, function(index, o) {
                        $('#course-class-body').append("<td><input type='checkbox' name='course_classes[" + o['id'] + "]' checked /></td>");
                    });
                    $('#course-class-body').append("</tr>");
                });

                presetFee();
            });
        }
    });

    $('input#course_registration_member_card_number').change(function() {
        var card_number = $(this).val();

        if (card_number == "") {
            $('#member-info').hide();
        } else {
            $.ajax({
                url: '/members.json?card_number='  + card_number
            }).done (function (data) {
                data = data[0];
                expired = Date.parse(data['latest_expiry_date']) < Date.now();
                member_type = data['member_type'];
                $('.member-fullname').html(data['fullname']);
                $('.member-chinese-name').html(data['chinese_name']);
                $('.member-expiry-date').html(data['latest_expiry_date']);
                $('.member-avatar').attr('src', data['avatar_url']);
                $('.member-type').html(data['member_type_string']);
                $('.member-link').attr('href', '/members/' + data['id']);
                if (expired) {
                  $('.member-expiry-date').addClass('error-message');
                  $('.member-expired-notice').show();
                } else {
                  $('.member-expiry-date').removeClass('error-message');
                  $('.member-expired-notice').hide();
                }

                $('#member-info').show();
                presetFee();
            });
        }
    });
});
