json.array! @book do |book|
    json.extract! book, :name, :description, :author
    json.on_loan book.book_rentals.on_loan.present?
    json.lost book.lost_damage
end
