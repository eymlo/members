json.array! @members do |member|
    json.extract! member, :id, :card_number, :fullname, :chinese_name, :latest_expiry_date, :member_type, :member_type_string
    json.avatar_url member.avatar.url(:medium)
end
