json.extract! @course, :id, :course_code, :course_name, :course_chinese_name, :course_description, :start_date, :end_date, :day_of_week, :day_of_week_string, :number_of_lessons, :course_fee_general, :course_fee_senior_circle, :course_fee_volunteer, :start_time_value, :end_time_value
json.course_instructor @course.course_instructor
json.course_classes do
  json.array! @course.course_classes, :date, :id
end
