class CourseInstructorsController < AuthenticatedController
  before_action -> { @session = session }
  before_action -> { @course_instructor = CourseInstructor.find(params[:id]) }, only: [:show, :update, :edit]

  skip_before_action :authenticate_user!, only: [:show, :index]

  def index
    set_instructors_grid
  end

  def new
  end

  def edit
  end

  def create
    @new_instructor = CourseInstructor.new(create_params)
    @new_instructor.save

    flash[:notice] = "#{@new_instructor.name} created successfully" if @new_instructor.errors.nil? || @new_instructor.errors.empty?
    set_instructors_grid
    respond_to do |format|
      format.js {}
      format.html {}
    end
  end

  def update
    @course_instructor.update!(create_params)

    flash[:notice] = "#{@course_instructor.name} updated successfully" if @course_instructor.errors.nil? || @course_instructor.errors.empty?
    set_instructors_grid
    respond_to do |format|
      format.js {}
      format.html {}
    end
  end

  def show
    @courses_grid = initialize_grid(@course_instructor.courses)
  end

  private
  def set_instructors_grid
    @course_instructors_grid = initialize_grid(CourseInstructor,
                                      name: 'instructors1')
  end

  def create_params
    params.require(:course_instructor).permit(:name, :chinese_name, :info)
  end
end
