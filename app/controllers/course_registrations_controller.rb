class CourseRegistrationsController < AuthenticatedController
  before_action :_find_course_registration, only: [:edit, :update]
  before_action -> { @session = session }
  before_action :authenticate_user!

  def new
    @courses = Course.where('end_date > ?', Time.now).order(:course_code)
    @day_of_week = nil
    if params[:day_of_week].present?
      @day_of_week = params[:day_of_week]
      @courses = @courses.where(day_of_week: params[:day_of_week])
    end
  end

  def update
    @course_registration.update(_update_course_registration_param)
    if @course_registration.errors.blank?
      flash[:notice] = 'Updated'
    end
  end

  def new_registration
    @course = Course.find(params[:course_id])
  end

  def edit
  end

  def create_registration
    @course_registration = course_registration = CourseRegistration.new(course_registration_param)
    course_registration.force = true
    course_registration.member = Member.find_by(
      card_number: params[:course_registration][:member][:card_number].upcase
    )

    course_registration.save

    if course_registration.errors.blank?
      course_registration.generate_course_classes
      flash[:notice] = "Successfully registered for #{course_registration.course.course_name}"
    else
      flash[:error] = "Failed to register. #{course_registration.errors.first.reduce('') {|x, y| x + ' ' + y.to_s }}"
    end
  end

  def create
    course_registration = CourseRegistration.new(course_registration_param)
    course_registration.member = Member.find_by(
      card_number: params[:course_registration][:member][:card_number].upcase
    )

    course_classes = params[:course_classes].select {|k, v| v =~ /on/i}.keys
    course_classes = CourseClass.where(id: course_classes)

    if course_classes.blank?
      flash[:error] = "One or more classes are needed"
      redirect_to action: :new and return
    end

    course_registration.save

    if course_registration.errors.blank?
      flash[:notice] = "Successfully registered for #{course_registration.course.course_name}"
    else
      flash[:error] = "Failed to register. #{course_registration.errors.first.reduce('') {|x, y| x + ' ' + y.to_s }}"
      redirect_to action: :new and return
    end

    course_classes.each do |cclass|
      CourseClassMembership.create!(member: course_registration.member,
                                   course_class: cclass)
    end

    redirect_to action: :new
  end

  def destroy
    registration = CourseRegistration.find(params[:id])
    course = registration.course
    registration.destroy!

    flash[:notice] = "Removed"
    redirect_to course
  end

  def index
    respond_to do |format|
      format.xls do
        spreadsheet = StringIO.new
        name, book = CourseRegistration.to_xls
        book.write spreadsheet
        send_data spreadsheet.string, :filename => name, :type =>  "application/vnd.ms-excel"
      end
    end
  end

  private

  def _update_course_registration_param
    params.require(:course_registration).permit(:receipt_number,
                                                :amount_paid,
                                                :remarks)
  end

  def course_registration_param
    params.require(:course_registration).permit(:course_id, :receipt_number, :amount_paid, :remarks)
  end

  def _find_course_registration
    @course_registration = CourseRegistration.find(params[:id])
  end
end
