class BooksController < AuthenticatedController
  skip_before_action :authenticate_user!, only: [:show, :index, :calendar, :today]

  before_action :_set_book, :only => [:show, :edit, :destroy, :update, :mark_as_returned]

  def index
    _set_grid
    @book = Book.where(book_code: params[:book_code]) unless params[:book_code].blank?

    respond_to do | format |
      format.html {}
      format.json {}
      format.xls {
        spreadsheet = StringIO.new
        name, book = Book.to_xls
        book.write spreadsheet
        send_data spreadsheet.string, :filename => name, :type =>  "application/vnd.ms-excel"
      }
    end
  end

  def show
    _set_rental_histories
  end

  def new
  end

  def edit
  end

  def mark_as_returned
    @book.mark_as_returned
    redirect_to :back
  end

  def update
    @book.update(_book_params)
    @book.save

    if @book.errors.nil? || @book.errors.empty?
      flash[:notice] = "#{@book.name} updated successfully"
    end

    _set_grid

  end

  def create
    @book = Book.new(_book_params)
    @book.save

    if @book.errors.nil? || @book.errors.empty?
      flash[:notice] = "#{@book.name} created successfully"
    end

    _set_grid
  end

  private

  def _set_rental_histories
    @rental_histories = initialize_grid(@book.book_rentals,
                                        include: [:member],
                                        order: 'borrowed_at',
                                        order_direction: 'desc')
  end

  def _set_book
    @book = Book.find(params[:id])
  end

  def _set_grid
    @books_grid = initialize_grid(Book,
                                  order: 'name')
  end

  def _book_params
    params.require(:book).permit(
      :name, :author, :description, :book_code, :lost_damage
    )
  end

end

