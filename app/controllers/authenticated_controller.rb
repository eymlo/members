class AuthenticatedController < ApplicationController
  before_action :authenticate_user!
  before_action -> { @authenticated = session[:authenticated] == true}

  def authenticate_user!
    redirect_to "/sessions/new?path=#{Rack::Utils.escape(request.path)}" if session[:authenticated] != true
  end
end
