class ActivitiesController < AuthenticatedController
  before_action :set_activity, only: [:update, :show, :edit]

  def index
    @activities_grid = initialize_grid(Activity.at_location(@location), include: [:location])
    respond_to do |format|
      format.html {}
      format.xls {
        spreadsheet = StringIO.new
        name, book = Activity.to_xls
        book.write spreadsheet
        send_data spreadsheet.string, :filename => name, :type =>  "application/vnd.ms-excel"
      }
    end
  end

  def edit
    respond_to do | format |
      format.html {}
    end
  end

  def show
    respond_to do | format |
      format.html {}
    end
  end

  def update
    [:name].each do | f |
      @activity[f] = activity_params[f] if activity_params[f]
    end
    @activity.save
    flash[:notice] = "#{@activity.name} updated successfully" if @activity.errors.nil? || @activity.errors.empty?
    respond_to do | format |
      format.js {}
    end
    @activities = Activity.all
    @activities_grid = initialize_grid(Activity)
  end

  def create
    @activity = Activity.new(new_activity_params)
    @activity.save
    flash[:notice] = "#{@activity.name} created successfully" if @activity.errors.nil? || @activity.errors.empty?
    @activities_grid = initialize_grid(Activity)
  end

  private

  def set_activity
    @activity = Activity.find(params[:id])
  end

  def activity_params
    params.require(:activity).permit(:name, :location_id)
  end

  def new_activity_params
    params.require(:activity).permit(:name, :location_id)
  end
end
