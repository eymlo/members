class ApplicationController < ActionController::Base
  before_filter :do_auth

  def do_auth
    http_basic_authenticate_with name: "circle", password: "senior", except: :index
  end

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :set_location

  def set_location
    loc = request.host.split('.')
    @location = loc.first
    @location = nil if Location.where(name: @location).blank?
  end
end
