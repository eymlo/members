class CoursesController < AuthenticatedController
  skip_before_action :authenticate_user!, only: [:show, :index, :calendar, :today]

  before_action -> { @session = session }
  before_action -> { @course = Course.find(params[:id]) }, only: [:show, :edit, :update]
  before_action -> { @course = Course.find(params[:course_id]) }, only: [:update_attendance, :regenerate_classes]

  def new
  end

  def edit
  end

  def duplicate
    existing_course = Course.find(params[:course_id])
    @course = existing_course.duplicate
    render :new
  end

  def index
    set_grid
  end

  def calendar
    respond_to do |format|
      format.json {
        @events = []
        if params[:start].present? && params[:end].present?
          @events = Course.at_location(@location).events(Date.parse(params[:start]), Date.parse(params[:end]))
        end
        render json: @events
      }
      format.html {}
    end
  end

  def create
    @course = Course.create(course_params)
    @course.start_time = Time.zone.parse(course_params[:start_time])
    @course.end_time = Time.zone.parse(course_params[:end_time])
    @course.generate_classes if @course.errors.blank?
    flash[:notice] = "#{@course.course_name} created successfully" if @course.errors.nil? || @course.errors.empty?
    set_grid
    respond_to do |format|
      format.js {}
      format.html {}
    end
  end

  def show
    set_members_grid
    set_course_attendance_grid
    set_course_class_grid
    @tab = params[:tab]
    respond_to do |format|
      format.json {}
      format.html {}
      format.xls do
        spreadsheet = StringIO.new
        name, book = @course.to_xls
        book.write spreadsheet
        send_data spreadsheet.string, :filename => name, :type =>  "application/vnd.ms-excel"
      end
    end
  end

  def update
    p = course_params
    p[:start_time] = Time.parse(course_params[:start_time])
    p[:end_time] = Time.parse(course_params[:end_time])
    @course.update(p)

    flash[:notice] = "#{@course.course_name} updated successfully" if @course.errors.nil? || @course.errors.empty?
    set_grid
    respond_to do |format|
      format.js {}
      format.html {}
    end
  end

  def today
    @classes = CourseClass.for_today(@location)
    @classes_grid = initialize_grid(@classes,
                                    include: {course: :location},
                                    order: 'start_time',
                                    order_direction: 'asc')
  end

  def update_attendance
    attendances = params[:attendance] || []
    begin
      existing_attendances = @course.course_attendances.to_a

      attendances.each do |cclass_id, members|
        cclass = CourseClass.find(cclass_id)
        existing_members = cclass.members
        members.select{|id, s| s}.each do |member_id, selected|
          member = Member.find(member_id)
          if existing_members.include? member
            existing_attendances.delete_if do |e|
              e.member == member && cclass_id.to_i == e.course_class.id
            end
          else
            CourseAttendance.create!({
              course_class: cclass,
              member: member
            })
          end
        end
      end

      existing_attendances.each do |e|
        e.destroy!
      end
    rescue => e
      flash[:error] = e.to_s
      redirect_to course_path(@course, tab: 'attendance') and return
    end

    flash[:notice] = 'Saved'
    redirect_to course_path(@course, tab: 'attendance') and return
  end

  def regenerate_classes
    raise "Cannot generate classes after it has started" if is_started?
    @course.course_classes.each {|a| a.mark_as_cancelled}
    @course.generate_classes
    redirect_to @course
  end

  private
  def set_members_grid
    @course_member_grid = initialize_grid(@course.course_registrations,
                                          name: 'member_grid',
                                          include: [:member],
                                          order: 'created_at',
                                          order_direction: 'desc')
  end

  def set_grid
    courses = Course.at_location(@location)
    unless params[:show_all] == 'true'
      courses = courses.where('end_date >= NOW()')
    end

    @courses_grid = initialize_grid(courses,
                                    include: [:location],
                                    order: 'start_date',
                                    order_direction: 'desc')
  end

  def set_course_class_grid
    @course_classes_grid = initialize_grid(@course.course_classes.active,
                                           name: 'class_grid',
                                           order: 'start_time',
                                           order_direction: 'desc')
  end

  def set_course_attendance_grid
    @course_attendances_grid = initialize_grid(@course.course_attendances,
                                               name: 'attendance_grid',
                                               order: 'created_at',
                                               order_direction: 'desc')
  end

  def course_params
    params.require(:course).permit(:course_code, :course_name, :day_of_week, :course_fee_general, :course_fee_senior_circle, :course_fee_volunteer, :start_date, :start_time, :end_date, :end_time ,:description, :number_of_lessons, :course_instructor_id, :course_description, :course_chinese_name, :location_id)
  end
end
