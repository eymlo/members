class CourseClassMembershipsController < AuthenticatedController
  before_action -> do
    @course = Course.find(params[:course_id])
    @member = Member.find(params[:id])
  end

  def edit
  end

  def update
    if params[:course_classes].blank?
      flash[:error] = 'At least one classes is needed'
      redirect_to @course and return
    end

    existing = CourseClassMembership.includes(:course_class).
      where(member: @member, course_classes: {course_id: @course.id}).to_a

    params[:course_classes].each do |cclass_id, status|
      if status == 'on'
        obj = CourseClassMembership.where(course_class_id: cclass_id,
                                          member: @member).to_a
        if obj.blank?
          obj ||= []
          obj << CourseClassMembership.create!(member: @member,
                                               course_class_id: cclass_id)
        end

        existing -= obj
      end
    end

    existing.each { |a| a.destroy }

    flash[:notice] = 'Saved.'
    redirect_to @course
  end
end
