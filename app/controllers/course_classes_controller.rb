class CourseClassesController < AuthenticatedController
  before_action -> { @course_class = CourseClass.find(params[:id]) }, except: [:new,
                                                                               :create,
                                                                               :attendance,
                                                                               :take_attendance]
  before_action -> { @course_class = CourseClass.find(params[:course_class_id]) },
    only: [:attendance, :take_attendance]
  before_action -> { @session = session }
  before_action -> { @course = Course.find(params[:course_id]) }

  def destroy
    @course_class.cancelled = true
    @course_class.save
    if @course_class.errors.blank?
      flash[:notice] = "Class archived successfully"
    else
      flash[:error] = "Class cannot be archived at the moment"
    end
    _set_course_class_grid
    respond_to do |format|
      format.js {}
      format.html {}
    end
  end

  def edit
  end

  def show
  end

  def new
  end

  def create
    @course_class = CourseClass.new(_create_course_class_params)
    @course_class.course = @course
    @course_class.cancelled = false
    @course_class.save
    flash[:notice] = "Class created successfully" if @course_class.errors.nil? || @course_class.errors.empty?
    _set_course_class_grid
    respond_to do |format|
      format.js {}
      format.html {}
    end
  end

  def update
    @course_class.update(_update_course_class_params)
    flash[:notice] = "Class updated successfully" if @course_class.errors.nil? || @course_class.errors.empty?
    _set_course_class_grid
    respond_to do |format|
      format.js {}
      format.html {}
    end
  end

  def attendance
  end

  def take_attendance
  end

  private
  def _update_course_class_params
    params.require(:course_class).permit(:start_time, :end_time)
  end

  def _create_course_class_params
    params.require(:course_class).permit(:start_time, :end_time)
  end

  def _set_course_class_grid
    @course.reload
    @course_classes_grid = initialize_grid(@course.course_classes.active,
                                order: 'start_time',
                                order_direction: 'desc')
  end


end
