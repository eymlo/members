class MembershipRegistrationsController < AuthenticatedController

  before_action -> { @member = Member.find(params[:member_id]) }
  before_action -> { @registration = MembershipRegistration.find(params[:id]) }, only: [:edit, :update]

  def new
  end

  def edit
  end

  def update
    MembershipRegistration.transaction do
      @registration.update(membership_registration_params)
      if @registration.errors.blank?
        @member[:latest_expiry_date] = [@registration.new_expiry_date, @member.latest_expiry_date].max
      end
      @member.save

      raise ActiveRecord::Rollback if !@member.errors.blank? || !@registration.errors.blank?
    end
    @member.reload
    flash[:notice] = "Successfully updated" if @registration.errors.blank?


    @renewals_grid = initialize_grid(MembershipRegistration.where(member: @member),
                                     include: [:member],
                                     order: 'year',
                                     order_direction: 'desc')
    respond_to do | format |
      format.js {}
    end
  end

  def create
    MembershipRegistration.transaction do
      @registration = MembershipRegistration.create(membership_registration_params)
      if @registration.errors.blank?
        @member[:latest_expiry_date] = [@registration.new_expiry_date, @member.latest_expiry_date].max
      end
      @member.save

      raise ActiveRecord::Rollback if !@member.errors.blank? || !@registration.errors.blank?
    end

    @member.reload
    flash[:notice] = "Successfully renewed" if @registration.errors.blank?

    @renewals_grid = initialize_grid(MembershipRegistration.where(member: @member),
                                     include: [:member],
                                     order: 'year',
                                     order_direction: 'desc')
    respond_to do | format |
      format.js {}
    end
  end

  private

  def membership_registration_params
    params.require(:membership_registration).permit(:year, :registration_date, :new_expiry_date, :receipt_number, :member_id)
  end
end
