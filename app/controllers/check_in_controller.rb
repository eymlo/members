class CheckInController < AuthenticatedController
  before_action :set_checkin, only: [:destroy, :edit, :update]
  skip_before_action :authenticate_user!, only: [:checkin_page, :checkin]
  skip_before_action :do_auth, :only => [:checkin_page]

  def checkin_page
    set_history
  end

  def checkin
    @not_registered = false
    @member = Member.find_by!(card_number: checkin_params.upcase)

    recent_count = CheckIn.where(member: @member).where("created_at >= ?", Time.now - 20.seconds).count +
      CourseAttendance.where(member: @member).where("created_at >= ?", Time.now - 20.seconds).count 

    if recent_count == 0
      if params[:checkin_activity_id] =~ /^a_/
        @activity = Activity.find_by(id: params[:checkin_activity_id].gsub(/^a_/, ''))
        unless @member.member_type == :R
          if @member.latest_expiry_date >= Time.now
            CheckIn.create({member: @member, activity_id: @activity.id})
          end
        else
          flash.now[:error] = "User is a regular user. Regular users are not allowed to attend activities"
        end
      elsif params[:checkin_activity_id] =~ /^c_/
        @course_class = CourseClass.find_by(id: params[:checkin_activity_id].gsub(/^c_/, ''))

        unless @course_class.is_member? @member
          @not_registered = true
          flash.now[:error] = "Member has not registered for this class"
        else
          CourseAttendance.create(course_class: @course_class, member: @member)
        end
      end
    end

    set_history

    respond_to do | format |
      format.js {}
    end
  end

  def histories
    set_checkin_grid
    respond_to do |format|
      format.html {}
      format.xls {
        spreadsheet = StringIO.new
        name, book = CheckIn.to_xls
        book.write spreadsheet
        send_data spreadsheet.string, :filename => name, :type =>  "application/vnd.ms-excel"
      }
    end
  end

  def destroy
    @checkin.destroy
    set_checkin_grid
    flash.now[:notice] = "History removed successfully" if @checkin.errors.nil? || @checkin.errors.empty?
  end

  def edit
  end

  def update
    @checkin.update(checkin_update_params)
    flash.now[:notice] = "History updated successfully" if @checkin.errors.nil? || @checkin.errors.empty?

    respond_to do | format |
      format.js {}
    end
    set_checkin_grid
  end

  private

  def set_checkin_grid
    @check_in_grid = initialize_grid(CheckIn.at_location(@location),
                                      include: [:member, :activity],
                                      order: 'created_at',
                                      order_direction: 'desc',
                                      name: 'task1')
  end

  def checkin_params
    params.require(:card_number)
  end

  def checkin_update_params
    params.require(:check_in).permit(:activity_id, :member_id, :created_at)
  end

  def set_history
    @history = ((CheckIn.at_location(@location).order(created_at: :desc).first(10) + CourseAttendance.at_location(@location).order(created_at: :desc).first(10)).sort {|a,b| b.created_at <=> a.created_at}).first(10).map do |c|
      if c.is_a? CheckIn
        Struct.new(:member, :created_at, :name).new(c.member, c.created_at, c.activity.name)
      elsif c.is_a? CourseAttendance
        Struct.new(:member, :created_at, :name).new(c.member, c.created_at, c.course.course_name)
      end
    end
  end

  def set_checkin
    @checkin = CheckIn.find(params[:id])
  end
end
