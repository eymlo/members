class BookRentalsController < AuthenticatedController
  skip_before_action :authenticate_user!

  def new
  end

  def edit
  end

  def update
  end

  def return_a_book
  end

  def do_return
    book = Book.where(:book_code => params[:book_code]).first

    if book.nil?
      flash[:error] = "Book cannot be found"
      redirect_to :back and return
    end

    book.mark_as_returned

    flash[:notice] = "#{book.name} is marked as returned"
    redirect_to :back and return
  end

  def create
    @book_rental = BookRental.new(
      :borrowed_at => Time.now,
      :expiring_at => Time.now + BookRental::RENTAL_PERIOD
    )
    @book_rental.member = Member.find_by(
      card_number: params[:book_rental][:member][:card_number].upcase
    )

    if @book_rental.member.nil?
      flash[:error] = 'Member not found'
      redirect_to :action => :new and return
    end

    if @book_rental.member.expired?
      flash[:error] = "Member (#{@book_rental.member.fullname}) is expired"
      redirect_to :action => :new and return
    end

    book = Book.find_by(
      book_code: params[:book_rental][:book][:book_code]
    )

    if book.lost_damage
      flash[:error] = "Book is lost/damanged"
      redirect_to :action => :new and return
    end

    @book_rental.book = book

    @book_rental.save

    if @book_rental.errors.blank?
      flash[:notice] = "Successfully rented #{@book_rental.book.name}"
    else
      flash[:error] = "Failed to rent #{@book_rental.errors.first.reduce('') {|x, y| x + ' ' + y.to_s }}"
    end

    redirect_to :action => :new
  end
end

