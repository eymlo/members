class MembersController < AuthenticatedController
  before_action -> { @session = session }
  before_action :set_member, only: [:show, :destroy, :update]
  before_action :set_member_with_member_id, only: [:edit, :destroy_confirm]
  skip_before_action :authenticate_user!, only: [:index]
  before_action :authenticate_user!, if: -> { self.action_name.to_sym != :index || self.request.format != :html }

  def create
    MembershipRegistration.transaction do
      @member = Member.new(new_member_params)
      @member.registration_date = registration_params[:registration_date]
      @member.latest_expiry_date = registration_params[:new_expiry_date]
      @member.issued_membership_card ||= false
      @member.save
      @registration = MembershipRegistration.new(registration_params)
      @registration.member = @member
      @registration.save
      raise ActiveRecord::Rollback if !@member.errors.blank? || !@registration.errors.blank?
    end

    @members = Member.all
    flash[:notice] = "#{@member.card_number} created successfully" if @member.errors.nil? || @member.errors.empty?
    set_members_grid
  end

  def destroy_confirm
  end

  def destroy
    unless params[:username] == 'admin' && params[:password] == 'circle'
      flash[:error] = "Invalid Credential"
      redirect_to action: :index and return
    end

    @member.destroy
    redirect_to action: :index and return
  end

  def edit
    @view = params[:view]
    respond_to do | format |
      format.html {}
    end
  end

  def show
    @previous = Member.where("card_number < ?", @member.card_number).order(card_number: :desc).first
    @next = Member.where("card_number > ?", @member.card_number).order(card_number: :asc).first
    @activities_grid = initialize_grid(CheckIn.where(member: @member), include: [:member, :activity], order: 'created_at', order_direction: 'desc')
    @renewals_grid = initialize_grid(MembershipRegistration.where(member: @member),
                                     include: [:member],
                                     order: 'year',
                                     order_direction: 'desc',)
    @course_registration_grid = initialize_grid(CourseRegistration.where(member: @member),
                                     include: [:member],
                                     order: 'created_at',
                                     order_direction: 'desc',)

    respond_to do | format |
      format.html {}
      format.js {}
      format.xls {
        spreadsheet = StringIO.new
        name, book = @member.to_xls
        book.write spreadsheet
        send_data spreadsheet.string, :filename => name, :type =>  "application/vnd.ms-excel"
      }
    end
  end

  def index
    @members = Member.all
    @members = @members.where(card_number: params[:card_number].upcase) unless params[:card_number].blank?
    set_members_grid

    respond_to do | format |
      format.html {}
      format.xls {
        spreadsheet = StringIO.new
        @members_grid.renderer = {}
        name, book = Member.to_xls(@members_grid.resultset)
        book.write spreadsheet
        send_data spreadsheet.string, :filename => name, :type =>  "application/vnd.ms-excel"
      }
      format.json {}
    end
  end

  def update
    @view = params[:member][:view]
    @member.update(member_params)
    flash[:notice] = "#{@member.card_number} updated successfully" if @member.errors.nil? || @member.errors.empty?
    respond_to do | format |
      format.js {}
    end
    @members = Member.all
    set_members_grid
  end

  private
  def set_members_grid
    @members_grid = initialize_grid(Member,
                                      name: 'members1')
  end

  def set_member
    @member = Member.find(params[:id])
  end

  def set_member_with_member_id
    @member = Member.find(params[:member_id])
  end

  def member_params
    params.require(:member).permit(:last_name, :first_name, :chinese_name, :issued_membership_card, :registration_date, :latest_expiry_date, :street, :city, :postal_code,
                 :home_phone, :cell_phone, :email_address, :emergency_contact,
                 :emergency_telephone_number, :emergency_contact_relationship,
                 :date_of_birth, :avatar, :notes, :card_number)
  end

  def registration_params
    params.require(:membership_registration).permit(:receipt_number, :year, :registration_date, :new_expiry_date)
  end

  def new_member_params
    params.require(:member).permit(:card_number, :last_name, :first_name, :chinese_name, :issued_membership_card, :registration_date, :latest_expiry_date, :street, :city, :postal_code,
                 :home_phone, :cell_phone, :email_address, :emergency_contact,
                 :emergency_telephone_number, :emergency_contact_relationship,
                 :date_of_birth, :avatar, :notes)
  end
end
