class CourseAttendancesController < AuthenticatedController
  before_action -> { @session = session }
  before_action -> { @course_attendance = CourseAttendance.find(params[:id]) }, only: [:edit, :update, :destroy]

  def edit
  end

  def update
    @course_attendance.update!(course_attendance_params)

    flash[:notice] = "Attendance updated successfully" if @course_attendance.errors.nil? || @course_attendance.errors.empty?
    set_course_attendances_grid
    respond_to do |format|
      format.js {}
      format.html {}
    end
  end

  def destroy
    @course_attendance.destroy

    flash[:notice] = "Attendance removed successfully" if @course_attendance.errors.nil? || @course_attendance.errors.empty?
    set_course_attendances_grid
    respond_to do |format|
      format.js {}
      format.html {}
    end
  end

  private
  def set_course_attendances_grid
    @course_attendances_grid = initialize_grid(@course_attendance.course.course_attendances,
                                    order: 'created_at',
                                    order_direction: 'desc',
                                    name: 'instructors1')
  end

  def course_attendance_params
    params.require(:course_attendance).permit(:member_id, :oucrse_id, :created_at)
  end
end
