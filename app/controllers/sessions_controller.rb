class SessionsController < ApplicationController
  def create
    if params[:username] == Members::Config.credential.username && params[:password] == Members::Config.credential.password
      session[:authenticated] = true
      @path = params[:path]
      @path = '/' if @path.blank?
    else
      flash[:error] = "Username/password is incorrect"
    end

    respond_to do | format |
      format.html {}
      format.js {}
    end
  end

  def destroy
    session[:authenticated] = nil
    redirect_to '/'
  end

  def new
    redirect_to '/' if session[:authenticated] == true
    @path = params[:path]
  end
end
