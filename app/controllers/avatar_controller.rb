class AvatarController < AuthenticatedController
  before_action -> { @member = Member.find(params[:member_id]) }, only: [:destroy]

  def destroy
    @view = 'member_info'
    @member.avatar = nil
    @member.save
    flash[:notice] = "#{@member.card_number} picture removed successfully" if @member.errors.nil? || @member.errors.empty?

    respond_to do |format|
      format.js {}
    end
  end
end
